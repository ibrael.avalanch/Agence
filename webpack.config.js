var webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var InstallPlugin = require('npm-install-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const HappyPack = require('happypack');


const merge = require('webpack-merge');
const glob = require('glob');



const parts = require('./webpack.parts');

const PATHS = {
    app: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'dist'),
    style: glob.sync('./src/**/*.css'),
};

console.log(process.env.PORT)

var commonConfig = merge([
    {
        entry: {
            app: ['babel-polyfill', PATHS.app],
            style: PATHS.style,
        },
        output: {
            path: PATHS.build,
            filename: '[name].js',
        },
        plugins: [

            new InstallPlugin({
                dev: function (module, path) {
                    return [
                            "babel-preset-react-hmre",
                            "webpack-dev-middleware",
                            "webpack-hot-middleware",
                        ].indexOf(module) !== -1;
                },

                peerDependencies: true,
            }),
        ],

        resolve : {
            extensions: [".js", ".jsx", ".json"]
        }
    },

    /*parts.dontParse({
        name: 'react',
        path: path.resolve(
            __dirname, 'node_modules/react/dist/react.min.js'
        )
    }),*/

    parts.loadFonts({
        options: {
            name: '[name].[ext]',
        },
    }),

])


const developmentConfig = merge([
    {
        plugins: [
            new DashboardPlugin(),

            new LodashModuleReplacementPlugin(),

            new webpack.NamedModulesPlugin(),
        ],

        output: {
            devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]',
        },
    },

  

    parts.loadCSS({
        include: PATHS.app
    }),

    parts.loadJavaScript({
        include: PATHS.app
    }),

    parts.devServer({
        // Customize host/port here if needed
        host: process.env.HOST,
        port: process.env.PORT,
    }),

    parts.loadImages(),

    parts.generateSourceMaps({ type: 'eval-source-map' }),

]);

const productionConfig = merge([

    {
        entry: {
            vendor: [ 'react', 'react-dom', 'redux', 'react-redux'],
        },

        performance: {
            hints: 'warning', // 'error' or false are valid too
            maxEntrypointSize: 100000, // in bytes
            maxAssetSize: 450000, // in bytes
        },

        resolve: {
            alias: {
                'react': 'react-lite',
                'react-dom': 'react-lite'
            }
        },

        plugins: [

            new LodashModuleReplacementPlugin(),

            new webpack.optimize.OccurrenceOrderPlugin(),

            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    drop_console: false,
                },

                mangle: false
            }),
        ]
    },

    parts.setFreeVariable(
        'process.env.NODE_ENV',
        'production'
    ),

    parts.clean(PATHS.build),

    parts.attachRevision(),

    parts.minifyJavaScript(),

    parts.minifyCSS({
        options: {
            discardComments: {
                removeAll: true,
            },
            // Run cssnano in safe mode to avoid
            // potentially unsafe transformations.
            safe: true,
        },
    }),

    parts.extractBundles([
        {
            name: 'vendor',

            minChunks: ({ resource }) => (
                resource &&
                resource.indexOf('node_modules') >= 0 &&
                resource.match(/\.js$/)
            ),
        },

        {
            name: 'manifest',
            minChunks: Infinity,
        },
    ]),

    parts.generateSourceMaps({ type: 'source-map' }),

    parts.extractCSS({
        use: [
            'css-loader',
            parts.autoprefix()
        ],
    }),

    parts.purifyCSS({
        paths: glob.sync(
            path.join(PATHS.app, '**', '*'),
            {nodir: true}
        ),
    }),

    parts.loadImages({
        options: {
            limit: 15000,
            name: '[name].[ext]',
        },
    }),


    parts.loadJavaScript({
        include: PATHS.app
    }),
]);


module.exports = function (env) {
    console.log('env', env);

    process.env.BABEL_ENV = env;


    const pages = [
        parts.page({
            title: 'Webpack demo',
            entry: {
                app: PATHS.app,
            },
            template: require.resolve(
                path.join(PATHS.app, 'index.html')
            ),

            chunks: ['app', 'manifest', 'vendor'],

        }),
    ];

    const config = env === 'production' ?
        productionConfig :
        developmentConfig;

    return merge([commonConfig, config].concat(pages))

};