const webpack = require('webpack');
var InstallPlugin = require('npm-install-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnano = require('cssnano');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');




exports.page = function({
    path = '',
    template = require.resolve(
        'html-webpack-plugin/default_index.ejs'
    ),
    title,
    chunks,
    entry
} = {}) {
    return {
        entry,
        plugins: [
            new HtmlWebpackPlugin({
                chunks,
                filename: `${path && path + '/'}index.html`,
                template,
                title,
            }),
        ],
    };
};

/**
 *
 * Set environment variables
 *
 * @param key
 * @param value
 * @returns {{plugins: *[]}}
 */
exports.setFreeVariable = function (key, value) {
    const env = {};
    env[key] = JSON.stringify(value);

    return {
        plugins: [
            new webpack.DefinePlugin(env),
        ],
    };
};


/**
 * Minify javascript code
 *
 * @returns {{plugins: *[]}}
 */
exports.minifyJavaScript = function () {
    return {
        plugins: [
            new BabiliPlugin(),
        ],
    };
};

/**
 * Attach a git revision to the build
 *
 * @returns {{plugins: *[]}}
 */
exports.attachRevision = function () {
    return {
        plugins: [
            new webpack.BannerPlugin({
                banner: new GitRevisionPlugin().version(),
            }),
        ],
    };
};

/**
 * Clean the build
 *
 * @param path
 * @returns {{plugins: *[]}}
 */
exports.clean = function (path) {
    return {
        plugins: [
            new CleanWebpackPlugin([path]),
        ],
    };
};


/***
 * Enblig sourcemaps
 *
 * @param type
 * @returns {{devtool: *}}
 */
exports.generateSourceMaps = function ({type}) {
    return {
        devtool: type,
    };
};

/**
 * Vendors chunks
 *
 * @param bundles
 * @returns {{plugins: (Array|*)}}
 */
exports.extractBundles = function (bundles) {
    return {
        plugins: bundles.map((bundle) => (
            new webpack.optimize.CommonsChunkPlugin(bundle)
        )),
    };
};

/**
 * Load javascript files
 *
 * @param include
 * @param exclude
 * @returns {{module: {rules: *[]}}}
 */
exports.loadJavaScript = function ({include, exclude}) {
    return {
        module: {
            rules: [
                {
                    test: /\.js|\.jsx$/,
                    include,
                    exclude,

                    use: [
                        'react-hot-loader',
                        {
                            loader: 'babel-loader',

                            options: {
                                // Enable caching for improved performance during
                                // development.
                                // It uses default OS directory by default. If you need
                                // something more custom, pass a path to it.
                                // I.e., { cacheDirectory: '<path>' }
                                cacheDirectory: true,
                            },
                        }
                    ],

                },
            ],
        },
    };
};


exports.dontParse = function ({name, path}) {
    const alias = {};
    alias[name] = path;

    return {
        module: {
            noParse: [
                new RegExp(path),
            ],
        },
        resolve: {
            alias,
        },
    };
};


/**
 * Development server
 *
 * @param host
 * @param port
 * @returns {{devServer: {historyApiFallback: boolean, hot: boolean, stats: string, host, port, overlay: {errors: boolean, warnings: boolean}}, plugins: *[]}}
 */
exports.devServer = function ({host, port} = {}) {
    return {

        devServer: {
            historyApiFallback: true,
            hot: true,
            inline: true,
            stats: 'errors-only',
            host, // Defaults to `localhost`
            port, // Defaults to 8080
            overlay: {
                errors: true,
                warnings: true,
            },
        },

        plugins: [
            new webpack.HotModuleReplacementPlugin(),

            // Ignore node_modules so CPU usage with poll
            // watching drops significantly.
            new webpack.WatchIgnorePlugin([
                path.join(__dirname, 'node_modules')
            ]),


        ],
    };
};

/**
 * Load css
 *
 * @param include
 * @param exclude
 * @returns {{module: {rules: *[]}}}
 */
exports.loadCSS = function ({include, exclude} = {}) {
    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'sass-loader'
                    ],
                    include,
                },
                {
                    test: /\.css$/,
                    include,
                    exclude,
                    use: [
                        'style-loader',
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: () => ([
                                    require('autoprefixer'),
                                    require('precss'),
                                    require('cssnext')
                                ]),
                            },
                        },
                    ],
                },
            ],
        },
    };
};

/**
 * Plug autoprefixer
 *
 * @returns {{loader: string, options: {plugins: (function(): *[])}}}
 */
exports.autoprefix = function () {
    return {
        loader: 'postcss-loader',
        options: {
            plugins: () => ([
                require('autoprefixer'),
            ]),
        },
    };
};

/**
 * Purify the css
 *
 * @param paths
 * @returns {{plugins: *[]}}
 */
exports.purifyCSS = function ({paths}) {
    return {
        plugins: [
            new PurifyCSSPlugin({paths: paths}),
        ],
    };
};

/**
 *
 * Extract css to files
 *
 * @param include
 * @param exclude
 * @param use
 * @returns {{module: {rules: *[]}, plugins: *[]}}
 */
exports.extractCSS = function ({include, exclude, use}) {
    // Output extracted CSS to a file
    const plugin = new ExtractTextPlugin({
        filename: '[name].css',
    });

    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ['style-loader', 'css-loader', 'sass-loader'],
                },
                {
                    test: /\.css$/,
                    include,
                    exclude,

                    use: plugin.extract({
                        use,
                        fallback: 'style-loader',
                    }),
                },
            ],
        },
        plugins: [plugin],
    };
};

/**
 * Minify the css
 *
 * @param options
 * @returns {{plugins: *[]}}
 */
exports.minifyCSS = function ({options}) {
    return {
        plugins: [
            new OptimizeCSSAssetsPlugin({
                cssProcessor: cssnano,
                cssProcessorOptions: options,
            }),
        ],
    };
};

/**
 * Load Images
 *
 * @param include
 * @param exclude
 * @param options
 * @returns {{module: {rules: *[]}}}
 */
exports.loadImages = function ({include, exclude, options} = {}) {
    return {
        module: {
            rules: [
                {
                    test: /\.(png|jpg)$/,
                    include,
                    exclude,

                    use: {
                        loader: 'url-loader',
                        options,
                    },
                },
                {
                    test: /\.svg$/,
                    use: 'file-loader',
                },
            ],
        },
    };
};

/**
 * Load typos (fonts)
 * @param include
 * @param exclude
 * @param options
 * @returns {{module: {rules: *[]}}}
 */
exports.loadFonts = function ({include, exclude, options} = {}) {
    return {
        module: {
            rules: [
                {
                    // Capture eot, ttf, woff, and woff2
                    test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                    include,
                    exclude,

                    use: {
                        loader: 'file-loader',
                        options,
                    },
                },
            ],
        },
    };
};