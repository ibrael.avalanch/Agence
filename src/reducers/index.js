import {combineReducers} from 'redux'
import ct from '../constants'


const app = (state = { title: 'Agence Dev'}, action) => {

    // code here

    return state
}


const search = ( state = { results: [], query: null }, action) => {

    //code
    return state
}


const projects = ( state = {
    list: [],
    has_more: true,
    offset: 0,
    limit: 10,
    loading: false,
    error: false
}, action) => {


    let { loading, error, list, has_more, offset, limit} = state

    switch(action.type) {

        case ct.LOADING_PROJECTS:
            loading = true 
            break;

        case ct.LOADED_PROJECTS:
            loading = false
            list = action.data
            break;

        case ct.LOADING_ERROR:
            error = true
            loading = false
            break;
    }



    return { loading, error, list, has_more, offset, limit }
}

export default combineReducers({
    app,
    search,
    projects
})