import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './index.css';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import configureStore from './store'
import {Provider} from 'react-redux'

injectTapEventPlugin();
var root = document.getElementById('root');

if(!root) {
    root = document.createElement('div');
    root.setAttribute('id', 'root');
    document.body.appendChild(root)
}


const render = Application => {
    ReactDOM.render(
        <Provider store={configureStore()} >
            <MuiThemeProvider>
                <Application />
            </MuiThemeProvider>
        </Provider>,
        root
    );
};

render(App);


if (module.hot) {
    module.hot.accept('./components/App' , () => render(App));
}