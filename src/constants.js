export default {

    LOADING_PROJECTS: 'LOADING_PROJECTS',

    LOADED_PROJECTS: 'LOADED_PROJECTS',

    LOADING_ERROR: 'LOADING_ERROR'
    
}