import 'whatwg-fetch'
import ct from '../constants'

export const load = () => {

   return dispatch => {
       
       dispatch({
           type: ct.LOADING_PROJECTS
       })
       
       return fetch ('data.json').then( res => res.json()).then( json => {
           
           dispatch({
               type: ct.LOADED_PROJECTS,
               data: json
           })
       }).catch( err => {

           console.log(err);

           dispatch({
               type: ct.LOADING_ERROR ,
               message: err.message
           })

       })
   } 
    
}