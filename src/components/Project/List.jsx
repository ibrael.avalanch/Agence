import React , { Component} from 'react'
import {connect} from 'react-redux'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import CircularProgress from 'material-ui/CircularProgress';
import './List.scss'

import {load} from '../../actions'

@connect (({ projects }) => projects )
export default class List extends Component {

    componentDidMount () {

        let { dispatch } = this.props

        dispatch(load())
    }

    render () {

        let { loading, limit, offset, list, has_more, error} = this.props

        return <section className="List">

            {loading ? <CircularProgress className="loader" /> : list.map( (project, index) =>
                <Card key={index} expandable={true} className="Card" >
                    <CardHeader
                        title={project.name}
                        subtitle={project.author}
                        avatar="https://www.material-ui.com/images/jsa-128.jpg"
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardMedia overlay={
                        <CardTitle title={project.name} subtitle={project.description.substring(0, 120) + '...'} />
                    } >
                        <img className="card__image card__image--loading" onLoad={ e => {
                            console.log(index, e.target)
                            e.target.setAttribute('class', 'card__image')
                        } } src={project.image } />
                    </CardMedia>

                    <CardTitle title={project.name} subtitle="Card subtitle" />
                    <CardText>
                        {project.description}
                    </CardText>
                    <CardActions>
                        <IconButton iconClassName="muidocs-icon-custom-github" />
                        <IconButton iconClassName="muidocs-icon-custom-github" disabled={true} />
                    </CardActions>
                </Card>
            , this)}


        </section>
    }
}