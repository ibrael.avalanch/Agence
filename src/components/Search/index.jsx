/**
 * Created by iespinosa on 15/03/2017.
 */

import React, {Component} from 'react'
import './Search.scss'

import AutoComplete from 'material-ui/AutoComplete';
import LinearProgress from 'material-ui/LinearProgress';


import {connect} from 'react-redux'


@connect( ({ search }) => search )
export default class Search extends Component {


    find (value) {
        console.log(value)
    }


    render () {

        let { results, query} = this.props

        console.log(this.props)

        return <section className="Search">
            <AutoComplete
                hintText="Type anything"
                dataSource={results}
                onUpdateInput={this.find.bind(this)}
                floatingLabelText="Trouver un projet"
                fullWidth={true}
            />
            <LinearProgress mode="indeterminate" />
        </section>
    }
}