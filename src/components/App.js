import React, { Component } from 'react';

import AppBar from 'material-ui/AppBar';
import {connect} from 'react-redux'
import List from './Project/List'

import logo from '../logo.svg';
import './App.scss';

import Search from './Search'


@connect(({app}) => app )
class App extends Component {
  render() {
      let { title} = this.props

      console.log(this.props)

    return (
      <div className="App">
          <AppBar
              title={title}
              iconClassNameRight="muidocs-icon-navigation-expand-more"
          />
          <Search />
          
          <List />
      </div>
    );
  }
}

export default App;
